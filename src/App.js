import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <p> Bienvenido a la prueba de React redux + router. Revisa la estructura del proyecto y borra este texto al comenzar. </p>
                    <p> Si lo prefieres, puedes crear o modificar la estructura para estar más cómodo. </p>
                    <p> ¡Buena suerte!</p>
                </div>
            </Provider>
        );
    }
}

export default App;